if ( language == nil ) then language = {} end

language = {
	suggested_font = "NotoSans-Bold.ttf",

	language = "Esperanto",

	game_title = "Fin 'n' Kit",
	game_creator = "Moosader LLC",
	game_website = "Moosader.com",
	game_year = "2016",
	game_version = "Dev Version 2016-08-08",

	game_title_1 = "Fin",
	game_title_2 = "'n'",
	game_title_3 = "Kit",

	mainmenu_play = "Ludi",
	mainmenu_exit = "Eliri",
	mainmenu_options = "Agordoj",
	mainmenu_help = "Helpoj",
	mainmenu_moremoose = "Pli da Moosader!",

	menu_tiles = "Kaheloj",
	menu_items = "Objektoj",
	menu_select = "Elekti",
	menu_options2 = "Agordoj",

	playmenu_story = "Rakonta reĝimo",
	playmenu_editor = "Niveleditilo",
	playmenu_custom = "Laŭmendaj niveloj",

    world_1 = "1-a Mondo",
    world_2 = "2-a Mondo",
    world_3 = "3-Mondo",
    world_4 = "4-a Mondo",
    world_5 = "5-a Mondo",

    world_1_name = "Agrabla Plaĝio",
    world_2_name = "Mistera Mondo",
    world_3_name = "Mistera Mondo",
    world_4_name = "Mistera Mondo",
    world_5_name = "Mistera Mondo",

    help_how_to_play = "Kiel ludi",
    help_credits = "Agnoskoj",

    options_sound_volume = "Sonforteco",
    options_music_volume = "Muzikforteco",
    options_language = "Ŝanĝi lingvon",

	editor_map_name = "Mapnomo",
	editor_save_map = "Konservi",
	editor_saved = "Konservita: ",
	editor_load_map = "Ŝarĝi",
	editor_load_map_name = "default.fin",
	editor_map_properties = "Ecoj",
	editor_file_mgmt = "Dosieradministrado",
	editor_clear_map = "Vakigi mapon",
	editor_back = "Eliri",
	editor_confirm_exit = "Eliri la editilon",
	editor_theme = "Temo",
	editor_theme_afternoon = "Vespero",
	editor_theme_sunset = "Noktiĝo",
    editor_trinkets = "Bagateloj",
    editor_obstacles = "Obstakloj",
    editor_misc_tiles = "Diversaj kaheloj",
    editor_extra_life = "Kroma Frapo",
    editor_level_end = "Nivelfino",
    editor_filename = "Dosiernomo",
    editor_background = "Fundo",
    editor_ground = "Grundo",
    editor_scroll = "Rapido de rulumado",
    editor_clear_map = "Vakigi mapon",
    editor_extra_life = "Kroma vivo",
    editor_level_end = "Markilo de nivelfino",
    editor_test_level = "Provo",
    editor_music = "Muziko",
    editor_clear_level = "Vaka",
    editor_behavior = "Konduto",
    editor_scale = "Skalo",
    editor_song_1 = "1-a Kanto",
    editor_song_2 = "2-a Kanto",
    editor_song_3 = "3-a Kanto",
    editor_scale_50 = "50%",
    editor_scale_100 = "100%",
    editor_scale_150 = "150%",
    editor_status_hello = "Saluton!",
    editor_status_set_default_behavior = "Ŝanĝis defaŭtan konduton",
    editor_status_set_default_scale = "Ŝanĝis defaŭtan skalon",
    editor_status_set_tile_behavior = "Konduto: ",
    editor_status_set_tile_scale = "Ŝanĝis skalon: ",
    editor_status_added_new_tile = "Aldonis kahelon: ",
    editor_status_removed_tile = "Forigis kahelon",
    editor_status_tile_count = "Totalo da kaheloj: ",
    editor_status_current_brush = "Nuna peniko: ",
    editor_status_updated_background = "Ĝisdatigis fundon",
    editor_status_updated_ground = "Ĝisdatigis grundon",
    editor_status_updated_song = "Ĝisdatigis kanton",
    editor_status_updated_speed = "Ĝisdatigis rapidon de rulumadon",

    game_menu_success = "Sukceson!",
    game_menu_failure = "Fiaskon!",
    game_menu_paused = "Paŭzo",
    game_menu_exit = "Eliri",
    game_menu_restart = "Rekomenci",
    game_menu_continue = "Daŭrigi",
    game_menu_next_level = "Sekvanta nivelo",

    credits_moosader_team = "La teamo de Moosader",
    credits_other = "Aliaj rimedoj",

    credits_name_racheljmorris = "Rachel Jane Morris",
    credits_role_racheljmorris = "Ĉefa programisto, ĉefa artisto",

    credits_name_teacoba = "Tea Coba",
    credits_role_teacoba = "Ĉefa tradukisto, niveldezajno",

    credits_name_jessicacapehart = "Jessica Capehart",
    credits_role_jessicacapehart = "Merkatiko, aŭdvidaĵoj",

    credits_name_rebekahhamilton = "Rebekah Hamilton",
    credits_role_rebekahhamilton = "Niveldezajno",

    credits_name_enmanueltoribio = "Enmanuel Toribio",
    credits_role_enmanueltoribio = "Programisto",

    credits_name_rosemorris = "Rose Morris",
    credits_role_rosemorris = "Aŭdvidaĵoj",

    credits_sound_effects_a = "PROVIZORAJ sonefikoj",
    credits_sound_effects_b = "BFXR.NET",

    credits_music_a = "PROVIZORA Muziko",
    credits_music_b = "Kevin MacLeod",
    credits_music_c = "Incompetech.com",
    credits_music_d = "Carnivale Intrigue",
    credits_music_e = "Mariachi Snooze",
    credits_music_f = "Motherlode",
    credits_music_g = "Thief In The Night",
    credits_music_h = "Upbeat Forever",

    credits_font_a = "Tiparo",
    credits_font_b = "Google Noto Sans, OFL",

    help_line1_mobile = "Tuŝetu Kit por saltigi ŝin.",
    help_line2_mobile = "Tuŝetu denove dum ŝi estas en la aero por resalti.",
    help_line3_mobile = "Tuŝetu Fin dum Kit estas en la aero por saltigi lin.",
    help_line4_mobile = "Se Kit sidas sur Fin, tuŝeti Fin saltigos Kit.",
    help_line5_mobile = "",
    help_line6_mobile = "",
    help_line7_mobile = "Kolektu bagatelojn kaj evitu obstaklojn.",
    help_line8_mobile = "Kolektu ĉiujn bagatelojn kaj evitu esti frapita por obteni 3 stelojn.",
    help_line9_mobile = "Kolektu korojn por obteni kroman frapon.",

    help_line1_pc = "Alkaklu Kit por saltigi ŝin.",
    help_line2_pc = "Klaku denove dum ŝi estas en la aero por resalti",
    help_line2b_pc = "",

    help_line3_pc = "Alklaku Fin dum Kit estas en la aero por saltigi lin.",
    help_line4_pc = "Se Kit sidas sur Fin, alklaki Fin saltigos Kit.",
    help_line4b_pc = "",
    help_line5_pc = "Vi povas uzi ankaŭ la SUBAN kaj SUPRAN sagoklavojn. ",
    help_line6_pc = "por saltigi Fin kaj Kit.",
    help_line7_pc = "Kolektu bagatelojn!",
    help_line8_pc = "Evitu la obstaklojn!",
    help_line9_pc = "Kolektu korojn por obteni kroman frapon.",
    help_line10_pc = "Obtenu 3 stelojn en ĉiu nivelo kolektante bagatelojn kaj evitante ĉiun damaĝon",
    help_line10b_pc = "",

    cutscene_w1_c1a = "En varma kaj sunplena plaĝo",
    cutscene_w1_c1b = "En varma kaj sunplena tago",
    cutscene_w1_c1c = "La katidino Kit iras fiŝi.",

    cutscene_w1_c2a = "Ŝi ne estas lerta",
    cutscene_w1_c2b = "ĉar ŝi ankoraŭ junas,",
    cutscene_w1_c2c = "sed ŝi estas persistema kaj neniam rezignas!.",

    cutscene_w1_c3a = "Post horoj da praktiko...",
    cutscene_w1_c5a = "...Kit obtenas ŝian premion!",
    cutscene_w1_c6a = "(Kun iom da helpo!)",

    cutscene_w1_c7a = "Fin diras \"Saluton!\"",
    cutscene_w1_c7b = "kaj Kit diras \"Sal'!\"",

    cutscene_w1_c8a = "Nova amikeco komenciĝis...",
    cutscene_w1_c8b = "...bazita sur ilia komuna amo por fiŝo",

    cutscene_w1_c9a = "Fin oferas kunporti Kit por trovi pli da fiŝo.",
    cutscene_w1_c9b = "Kit ensaltas, ekscitite.",

    cutscene_w1_c10a = "Atendu nin fiŝoj, ni iras por vi!",
}

-- [[ Tea's Haiku:
    Anasoj naĝas
    Jen panon ĝoje manĝas
		Somera amo]]
