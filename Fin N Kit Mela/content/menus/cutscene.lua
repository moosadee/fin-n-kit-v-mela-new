assets = {
    { name = "background_art", path = "content/graphics/ui/bg_cutscene.png" },
    { name = "btn_cutscene_next",    path = "content/graphics/ui/btn_cutscene_next.png" },
    { name = "btn_cutscene_next2",    path = "content/graphics/ui/btn_cutscene_next2.png" },

    { name = "slide_page1", path = "content/graphics/cutscenes/w1_c1.png" },
    { name = "slide_page2", path = "content/graphics/cutscenes/w1_c2.png" },
    { name = "slide_page3", path = "content/graphics/cutscenes/w1_c3.png" },
    { name = "slide_page4", path = "content/graphics/cutscenes/w1_c4.png" },
    { name = "slide_page5", path = "content/graphics/cutscenes/w1_c5.png" },
    { name = "slide_page6", path = "content/graphics/cutscenes/w1_c6.png" },
    { name = "slide_page7", path = "content/graphics/cutscenes/w1_c7.png" },
    { name = "slide_page8", path = "content/graphics/cutscenes/w1_c8.png" },
    { name = "slide_page9", path = "content/graphics/cutscenes/w1_c9.png" },
    { name = "slide_page10", path = "content/graphics/cutscenes/w1_c10.png" },
}

menu_options = {
    total_pages = 10
}

scrw = 1280
scrh = 720

cutw = 937
cuth = 631
cutx = 50
cuty = 50

texth = 50

elements = {
    -- Common
    { ui_type = "image", id = "zbackground", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh },
    { ui_type = "button", id = "btn_cutscene_next",  texture_id = "btn_cutscene_next", texture_id_2 = "btn_cutscene_next2", x = 1070, y = 250, width = 188, height = 206, effect = "animate", effect_speed = 2 },

    { page = 1,  ui_type = "image", id = "page1",  texture_id = "slide_page1", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 2,  ui_type = "image", id = "page2",  texture_id = "slide_page2", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 3,  ui_type = "image", id = "page3",  texture_id = "slide_page3", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 4,  ui_type = "image", id = "page4",  texture_id = "slide_page4", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 5,  ui_type = "image", id = "page5",  texture_id = "slide_page5", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 6,  ui_type = "image", id = "page6",  texture_id = "slide_page6", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 7,  ui_type = "image", id = "page7",  texture_id = "slide_page7", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 8,  ui_type = "image", id = "page8",  texture_id = "slide_page8", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 9,  ui_type = "image", id = "page9",  texture_id = "slide_page9", x = cutx, y = cuty, width = cutw, height = cuth },
    { page = 10, ui_type = "image", id = "page10", texture_id = "slide_page10", x = cutx, y = cuty, width = cutw, height = cuth },

    { page = 1, ui_type = "label", id = "cutscene_w1_c1a", text_id = "cutscene_w1_c1a", x = cutx + 400, y = cuty + 20, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 1, ui_type = "label", id = "cutscene_w1_c1b", text_id = "cutscene_w1_c1b", x = cutx + 400, y = cuty + 20 + texth, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 1, ui_type = "label", id = "cutscene_w1_c1c", text_id = "cutscene_w1_c1c", x = cutx + 400, y = cuty + 20 + texth * 2, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },


    { page = 2, ui_type = "label", id = "cutscene_w1_c2a", text_id = "cutscene_w1_c2a", x = cutx + 30, y = cuty + 20, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 2, ui_type = "label", id = "cutscene_w1_c2b", text_id = "cutscene_w1_c2b", x = cutx + 30, y = cuty + 20 + texth, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 2, ui_type = "label", id = "cutscene_w1_c2c", text_id = "cutscene_w1_c2c", x = cutx + 30, y = cuty + 20 + texth * 2, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { page = 3, ui_type = "label", id = "cutscene_w1_c3a", text_id = "cutscene_w1_c3a", x = cutx + 30, y = cuty + 20, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { page = 5, ui_type = "label", id = "cutscene_w1_c5a", text_id = "cutscene_w1_c5a", x = cutx + 30, y = cuty + 20, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { page = 6, ui_type = "label", id = "cutscene_w1_c6a", text_id = "cutscene_w1_c6a", x = cutx + 500, y = cuty + 300, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { page = 7, ui_type = "label", id = "cutscene_w1_c7a", text_id = "cutscene_w1_c7a", x = cutx + 30, y = cuty + 100, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 7, ui_type = "label", id = "cutscene_w1_c7b", text_id = "cutscene_w1_c7b", x = cutx + 500, y = cuty + 300, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { page = 8, ui_type = "label", id = "cutscene_w1_c8a", text_id = "cutscene_w1_c8a", x = cutx + 100, y = cuty + 30, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 8, ui_type = "label", id = "cutscene_w1_c8b", text_id = "cutscene_w1_c8b", x = cutx + 100, y = cuty + 500, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { page = 9, ui_type = "label", id = "cutscene_w1_c9a", text_id = "cutscene_w1_c9a", x = cutx + 30, y = cuty + 30, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 9, ui_type = "label", id = "cutscene_w1_c9b", text_id = "cutscene_w1_c9b", x = cutx + 30, y = cuty + 400, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { page = 10, ui_type = "label", id = "cutscene_w1_c10a", text_id = "cutscene_w1_c10a", x = cutx + 200, y = cuty + 400, width = 0, height = texth, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },


}
