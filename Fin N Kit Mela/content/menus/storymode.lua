assets = {
    { name = "background_art",      path = "content/graphics/ui/mainmenu_background.png" },
    { name = "button_main",         path = "content/graphics/ui/button_main.png" },
    { name = "button_sub",          path = "content/graphics/ui/button_sub.png" },
    { name = "button_fb",           path = "content/graphics/ui/button_social_facebook.png" },
    { name = "button_tw",           path = "content/graphics/ui/button_social_twitter.png" },
    { name = "button_left",         path = "content/graphics/ui/button_nav_left.png" },
    { name = "locked",              path = "content/graphics/ui/locked.png" },
    { name = "scroll_buttons",      path = "content/graphics/ui/world_scroll_buttons.png" },
    { name = "world_1",             path = "content/graphics/ui/world_1.png" },
    { name = "world_locked",        path = "content/graphics/ui/world_locked.png" },
}

menu_options = {
    total_pages = 5
}

button1y = 45
button2y = 220
button3y = 395
buttonx = 50
buttonx2 = 640

buttonw = 580
buttonh = 100

scrollbuttonw = 109
scrollbuttonh = 217
scrollbuttonx1 = 1280 * 1/4 - scrollbuttonw/2 - 100
scrollbuttonx2 = 1280 * 3/4 - scrollbuttonw/2 + 100
scrollbuttony = 720/2 - scrollbuttonh/2

worldw = 493
worldh = 575
worldx = 1280/2 - worldw/2
worldy = 720/2 - worldh/2
labely = 450
sublabely = 425
labelh = 75
sublabelh = 35

elements = {
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh },

    { ui_type = "button", id = "btn_back_to_play",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { ui_type = "button", id = "btn_prev_world",  texture_id = "scroll_buttons", x = scrollbuttonx1, y = scrollbuttony, width = scrollbuttonw, height = scrollbuttonh, frame_x = 0, frame_y = 0, frame_width = scrollbuttonw, frame_height = scrollbuttonh },
    { ui_type = "button", id = "btn_next_world",  texture_id = "scroll_buttons", x = scrollbuttonx2, y = scrollbuttony, width = scrollbuttonw, height = scrollbuttonh, frame_x = scrollbuttonw, frame_y = 0, frame_width = scrollbuttonw, frame_height = scrollbuttonh },

    -- World select: World 1
    { page = 1, ui_type = "button",     id = "btn_world_1",         texture_id = "world_1", x = worldx, y = worldy, width = worldw, height = worldh },
    { page = 1, ui_type = "label",      id = "lbl_world_1_name",    text_id = "world_1_name", font_id = "main", x = worldx+10, y = worldy+labely, width = worldw, height = labelh, font_r = 0, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },
    { page = 1, ui_type = "label",      id = "lbl_world_1",         text_id = "world_1", font_id = "main", x = worldx+10, y = worldy+sublabely, width = worldw, height = sublabelh, font_r = 255, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },

    -- World select: World 2
    { page = 2, ui_type = "button",     id = "btn_world_2",         texture_id = "world_locked", x = worldx, y = worldy, width = worldw, height = worldh },
    { page = 2, ui_type = "label",      id = "lbl_world_2_name",    text_id = "world_2_name", font_id = "main", x = worldx+10, y = worldy+labely, width = worldw, height = labelh, font_r = 0, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },
    { page = 2, ui_type = "label",      id = "lbl_world_2",         text_id = "world_2", font_id = "main", x = worldx+10, y = worldy+sublabely, width = worldw, height = sublabelh, font_r = 255, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },

    -- World select: World 3
    { page = 3, ui_type = "button",     id = "btn_world_3",         texture_id = "world_locked", x = worldx, y = worldy, width = worldw, height = worldh },
    { page = 3, ui_type = "label",      id = "lbl_world_3_name",    text_id = "world_3_name", font_id = "main", x = worldx+10, y = worldy+labely, width = worldw, height = labelh, font_r = 0, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },
    { page = 3, ui_type = "label",      id = "lbl_world_3",         text_id = "world_3", font_id = "main", x = worldx+10, y = worldy+sublabely, width = worldw, height = sublabelh, font_r = 255, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },

    -- World select: World 4
    { page = 4, ui_type = "button",     id = "btn_world_4",         texture_id = "world_locked", x = worldx, y = worldy, width = worldw, height = worldh },
    { page = 4, ui_type = "label",      id = "lbl_world_4_name",    text_id = "world_4_name", font_id = "main", x = worldx+10, y = worldy+labely, width = worldw, height = labelh, font_r = 0, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },
    { page = 4, ui_type = "label",      id = "lbl_world_4",         text_id = "world_4", font_id = "main", x = worldx+10, y = worldy+sublabely, width = worldw, height = sublabelh, font_r = 255, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },

    -- World select: World 5
    { page = 5, ui_type = "button",     id = "btn_world_5",         texture_id = "world_locked", x = worldx, y = worldy, width = worldw, height = worldh },
    { page = 5, ui_type = "label",      id = "lbl_world_5_name",    text_id = "world_5_name", font_id = "main", x = worldx+10, y = worldy+labely, width = worldw, height = labelh, font_r = 0, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },
    { page = 5, ui_type = "label",      id = "lbl_world_5",         text_id = "world_5", font_id = "main", x = worldx+10, y = worldy+sublabely, width = worldw, height = sublabelh, font_r = 255, font_g = 0, font_b = 0, font_a = 255, use_shadow = 0 },
}
