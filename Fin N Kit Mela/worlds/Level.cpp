#include "Level.hpp"

#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/utilities/Logger.hpp"
#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/base/Application.hpp"

#include <algorithm>
#include <cmath>

Level::Level()
{
    m_offset = 0;
    m_backgrounds[0].SetPosition( kuko::FloatRect( 0, 0, 1280, 720 ) );
    m_backgrounds[1].SetPosition( kuko::FloatRect( 1280, 0, 1280, 720 ) );

    m_grounds[0].SetPosition( kuko::FloatRect( 0, 0, 1280, 111 ) );
    m_grounds[1].SetPosition( kuko::FloatRect( 1280, 0, 1280, 111 ) );

    m_grounds[2].SetPosition( kuko::FloatRect( 0, 0, 1280, 184 ) );
    m_grounds[3].SetPosition( kuko::FloatRect( 1280, 0, 1280, 184 ) );

    m_groundId = 1;
    m_backgroundId = 1;
    m_songId = 1;
    m_scrollSpeed = 1;
    m_editorState = false;
}

Level::~Level()
{
    Clear();
}

void Level::Clear()
{
    // Free memory first
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        if ( m_levelObjects[i] != NULL )
        {
            delete m_levelObjects[i];
            m_levelObjects[i] = NULL;
        }
    }

    m_levelObjects.clear();
}

void Level::Begin()
{
    kuko::SoundManager::PlayMusic( m_song, true );

    m_offset = 0;

    int h = kuko::Application::GetDefaultHeight();

    m_backgrounds[0].SetPosition( kuko::FloatRect( 0, 0, 1280, 720 ) );
    m_backgrounds[1].SetPosition( kuko::FloatRect( 1280, 0, 1280, 720 ) );

    m_grounds[0].SetPosition( kuko::FloatRect( 0, h-184, 1280, 184 ) );
    m_grounds[1].SetPosition( kuko::FloatRect( 1280, h-184, 1280, 184 ) );

    m_grounds[2].SetPosition( kuko::FloatRect( 0, h-111, 1280, 111 ) );
    m_grounds[3].SetPosition( kuko::FloatRect( 1280, h-111, 1280, 111 ) );
}

void Level::SetPath( const std::string& path )
{
    m_levelPath = path;
}

void Level::SetTitle( const std::string& title )
{
    m_title = title;
}

unsigned int Level::GetTileCount()
{
    return m_levelObjects.size();
}

int Level::AddTile( Object* obj )
{
    m_levelObjects.push_back( obj );
    return m_levelObjects.size() - 1;
}

Object& Level::GetTile( int index )
{
    return *m_levelObjects[ index ];
}

Object* Level::GetTilePtr( int index )
{
    return m_levelObjects[ index ];
}

void Level::RemoveTile( int index )
{
    m_levelObjects.erase( m_levelObjects.begin() + index );
}

void Level::AddBackgroundTexture( SDL_Texture* texture )
{
    m_backgrounds[0].SetTexture( texture );
    m_backgrounds[1].SetTexture( texture );
}

void Level::SetGroundTextures( SDL_Texture* txtBackground, SDL_Texture* txtForeground )
{
    m_grounds[0].SetTexture( txtBackground );
    m_grounds[1].SetTexture( txtBackground );
    m_grounds[2].SetTexture( txtForeground );
    m_grounds[3].SetTexture( txtForeground );

    int screenHeight = kuko::Application::GetDefaultHeight();

    int w, h;
    SDL_QueryTexture( txtBackground, NULL, NULL, &w, &h );
    m_grounds[0].SetPosition( kuko::FloatRect( 0, screenHeight - h, w, h ) );
    m_grounds[1].SetPosition( kuko::FloatRect( 0, screenHeight - h, w, h ) );

    SDL_QueryTexture( txtForeground, NULL, NULL, &w, &h );
    m_grounds[2].SetPosition( kuko::FloatRect( 0, screenHeight - h, w, h ) );
    m_grounds[3].SetPosition( kuko::FloatRect( 0, screenHeight - h, w, h ) );
}

void Level::ScrollBackground( float amount )
{
    float speeds[3] = { amount * 1/2, amount * 1/3, amount * 1/4 };



    // Move waves
    for ( int i = 0; i < 4; i += 2 )
    {
        kuko::FloatRect pos = m_grounds[i].GetPosition();

        if ( i == 0 || i == 1 )
        {
            pos.x += speeds[1];
        }
        else
        {
            pos.x += speeds[0];
        }

        if ( pos.x <= -pos.w )
        {
            pos.x = 0;
        }
        m_grounds[i].SetPosition( pos );
    }

    for ( int i = 1; i < 4; i += 2 )
    {
        kuko::FloatRect pos = m_grounds[i].GetPosition();
        kuko::FloatRect prevPos = m_grounds[i-1].GetPosition();

        pos.x = prevPos.x + pos.w;
        m_grounds[i].SetPosition( pos );
    }

    // Move background
    kuko::FloatRect bgMain = m_backgrounds[0].GetPosition();
    kuko::FloatRect bgSub = m_backgrounds[1].GetPosition();

    bgMain.x += speeds[2];
    if ( bgMain.x < -bgMain.w )
    {
        bgMain.x = 0;
    }

    bgSub.x = bgMain.x + bgMain.w;

    m_backgrounds[0].SetPosition( bgMain );
    m_backgrounds[1].SetPosition( bgSub );
}

void Level::Draw( float scrollOffset, float scale )
{
    // Backgrounds
    m_backgrounds[0].Draw();
    m_backgrounds[1].Draw();
    m_grounds[0].Draw();
    m_grounds[1].Draw();

    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        m_levelObjects[i]->Draw();
    }
}

void Level::Draw( float x, float y, float scale )
{
    // Backgrounds
    m_backgrounds[0].Draw();
    m_backgrounds[1].Draw();
    m_grounds[0].Draw();
    m_grounds[1].Draw();

    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        m_levelObjects[i]->Draw();
    }
}

void Level::DrawForeground( float scrollOffset, float scale )
{
    m_grounds[2].Draw();
    m_grounds[3].Draw();
}

void Level::DrawForeground( float x, float y, float scale )
{
    m_grounds[2].Draw();
    m_grounds[3].Draw();
}

void Level::SetBackground( int val )
{
    m_backgroundId = val;
}

void Level::SetGround( int val )
{
    m_groundId = val;
}

void Level::SetSong( int val )
{
    m_songId = val;
}

int Level::GetSongID()
{
    return m_songId;
}

void Level::Save( const std::string& filename )
{
    std::string path = StringUtil::RemoveWhitespace( m_levelPath ) + StringUtil::RemoveWhitespace( filename );
    Logger::Out( "Save level to \"" + path + "\"", "Level::Save" );

    std::ofstream output( path );
    std::vector<std::string> assetPaths;
    std::vector<std::string> assetNames;

    output << "MAP_VERSION " << kuko::LanguageManager::Text( "game_version" ) << std::endl;
    output << "LEVEL_NAME" << std::endl;
    if ( m_title == "" )
    {
        m_title = "default";
    }
    output << m_title << std::endl;

    // Create list of assets that should be loaded
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        std::string fileForImage = kuko::ImageManager::GetTextureFile( m_levelObjects[i]->GetType() );
        // Add to list of assets if not already in the list
        std::vector<std::string>::iterator it;
        it = std::find( assetPaths.begin(), assetPaths.end(), fileForImage );
        if ( it == assetPaths.end() )
        {
            // Add to list
            assetNames.push_back( m_levelObjects[i]->GetType() );
            assetPaths.push_back( fileForImage );
        }
    }

    output << "ASSETS" << std::endl;
    for ( unsigned int i = 0; i < assetNames.size(); i++ )
    {
        output << assetPaths[i] << std::endl;
        output << assetNames[i] << std::endl;
    }
    output << "END" << std::endl;

    output << "LEVEL PROPERTIES" << std::endl;
    output << "BACKGROUND " << m_backgroundId << std::endl;
    output << "GROUND     " << m_groundId << std::endl;
    output << "SONG       " << m_songId << std::endl;
    output << "SPEED      " << m_scrollSpeed << std::endl;
    output << "END" << std::endl;

    output << "TILES" << std::endl;
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        kuko::FloatRect pos = m_levelObjects[i]->GetDefaultPosition();

        output << i
            << " TYPE " << m_levelObjects[i]->GetType()
            << " X " << pos.x
            << " Y " << pos.y
            << " BEHAVIOR " << m_levelObjects[i]->GetBehavior()
            << " SCALE " << m_levelObjects[i]->GetScale()
            << std::endl;
    }
    output << "END" << std::endl;

    output.close();
}

void Level::Load( const std::string& filename )
{
    std::string path = StringUtil::RemoveWhitespace( m_levelPath ) + StringUtil::RemoveWhitespace( filename );
    Logger::Out( "Load level from \"" + path + "\"", "Level::Load" );

    std::ifstream input( path.c_str() );

    std::string buffer;
    input.ignore();
    getline( input, buffer ); // MAP_VERSION
    getline( input, buffer ); // LEVEL_NAME
    getline( input, m_title );
    getline( input, buffer ); // ASSETS

    std::string assetPath;
    std::string assetName;

    int assetCount = 0;
    while ( input >> assetPath )
    {
        if ( assetPath == "END" )
        {
            break;
        }
        input >> assetName;
        kuko::ImageManager::AddTexture( assetName, assetPath );
        assetCount++;
    }
    Logger::Out( StringUtil::IntToString( assetCount ) + " assets loaded", "Level::Load" );

    getline( input, buffer ); // LEVEL PROPERTIES
    while ( input >> buffer )
    {
        if ( buffer == "END" )
        {
            break;
        }

        if ( buffer == "BACKGROUND" )
        {
            input >> m_backgroundId;
        }
        else if ( buffer == "GROUND" )
        {
            input >> m_groundId;
        }
        else if ( buffer == "SONG" )
        {
            input >> m_songId;
        }
        else if ( buffer == "SPEED" )
        {
            input >> m_scrollSpeed;
        }
    }

    std::string tileName;
    std::string tileType;
    std::string xlabel;
    std::string ylabel;
    std::string typeLabel;
    std::string behaviorLabel;
    std::string scaleLabel;
    float x, y;
    int behavior;
    float scale;

    input >> buffer; // TILES
    // 0 TYPE trinket_balloon X 320 Y 160
    while ( input >> buffer ) // number
    {
        if ( buffer == "END" )
        {
            break;
        }

        input >> typeLabel // TYPE
            >> tileType
            >> xlabel // X
            >> x
            >> ylabel // Y
            >> y
            >> behaviorLabel // BEHAVIOR
            >> behavior
            >> scaleLabel // SCALE
            >> scale;

        Object* object = new Object;
        object->SetType( tileType );
        kuko::FloatRect pos;
        pos.x = x;
        pos.y = y;
        pos.w = 80;
        pos.h = 80;
        kuko::FloatRect col = { 0, 0, 80, 80 };
        object->Setup( tileType, kuko::ImageManager::GetTexture( tileType ), pos );
        object->SetCollisionRegion( col );
        object->SetBehavior( Behavior( behavior ) );
        object->SetScale( scale );
        object->SetSpeed( m_scrollSpeed * 1000 );
        object->SetIsInEditor( m_editorState );
        m_levelObjects.push_back( object );
    }

    input.close();
    Logger::Out( StringUtil::IntToString( m_levelObjects.size() ) + " tiles loaded", "Level::Load" );

    // Set up the background / ground / song
    UpdateProperties();
}

void Level::Update()
{
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        m_levelObjects[i]->Update();
    }
}

ItemCategory Level::Collision( const kuko::BaseEntity& entity )
{
    int removeIndex = -1;
    ItemCategory hit = NONE;
    // Check collision with all objects
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        float distance = GetDistance( entity.GetPosition(), m_levelObjects[i]->GetPosition() );
//        if ( distance < m_levelObjects[i]->GetPosition().w )
//        {
//            hit = m_levelObjects[i]->GetCategory();
//            removeIndex = i;
//            break;
//        }

        if ( m_levelObjects[i]->IsCollision( entity ) )
        {
            // Remove this item
            hit = m_levelObjects[i]->GetCategory();
            removeIndex = i;
            break;
        }
    }

    if ( removeIndex != -1 )
    {
        m_levelObjects.erase( m_levelObjects.begin() + removeIndex );
    }

    return hit;
}

int Level::GetRemainingTrinketCount()
{
    int count = 0;
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        if ( m_levelObjects[i]->GetCategory() == TRINKET )
        {
            count++;
        }
    }
    return count;
}

void Level::Scroll( float amount )
{
    amount *= m_scrollSpeed;
    m_offset += amount;

    // Move all tiles
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        m_levelObjects[i]->Scroll( amount );
    }
}

void Level::EditorScroll( float amount )
{
    m_offset += amount;

    // Move all tiles
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
       kuko::FloatRect pos = m_levelObjects[i]->GetPosition();
       pos.x += amount;
       m_levelObjects[i]->SetPosition( pos );
    }
}

void Level::SetScroll( float amount )
{
    amount *= kuko::Application::GetTimeStep();
    m_offset = amount;

    // Move all tiles
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
       kuko::FloatRect pos = m_levelObjects[i]->GetPosition();
       pos.x += amount;
       m_levelObjects[i]->SetPosition( pos );
    }
}

void Level::UpdateProperties()
{
    std::string background;
    std::string groundA;
    std::string groundB;
    std::string song;

    if ( m_backgroundId == 1 )
    {
        background = "background_sand";
    }
    else if ( m_backgroundId == 2 )
    {
        background = "background_tank";
    }
    else if ( m_backgroundId == 3 )
    {
        background = "background_night";
    }

    if ( m_groundId == 1 )
    {
        groundA = "background_waves";
        groundB = "foreground_waves";
    }
    else if ( m_groundId == 2 )
    {
        groundA = "background_waves2";
        groundB = "foreground_waves2";
    }
    else if ( m_groundId == 3 )
    {
        groundA = "background_waves3";
        groundB = "foreground_waves3";
    }

    if ( m_songId == 1 )
    {
        m_song = "level1";
    }
    else if ( m_songId == 2 )
    {
        m_song = "level2";
    }
    else if ( m_songId == 3 )
    {
        m_song = "level3";
    }

    kuko::ImageManager::AddTexture( background, "content/graphics/game/" + background + ".png"  );
    AddBackgroundTexture( kuko::ImageManager::GetTexture( background ) );

    kuko::ImageManager::AddTexture( groundA, "content/graphics/game/" + groundA + ".png" );
    kuko::ImageManager::AddTexture( groundB, "content/graphics/game/" + groundB + ".png" );

    SetGroundTextures( kuko::ImageManager::GetTexture( groundA ), kuko::ImageManager::GetTexture( groundB ) );
}

void Level::SetScrollSpeed( float val )
{
    m_scrollSpeed = val;
}

float Level::GetScrollSpeed()
{
    return m_scrollSpeed;
}

void Level::TileDebugOut()
{
    // Verify
    Logger::Out( "Verify tiles loaded", "Level::TileDebugOut" );
    for ( unsigned int i = 0; i < m_levelObjects.size(); i++ )
    {
        Logger::Debug( "OBJECT " + StringUtil::IntToString( i )
            + ", Type:     " + StringUtil::IntToString( m_levelObjects[i]->GetCategory() )
            + ", Behavior: " + Object::GetBehaviorName( m_levelObjects[i]->GetBehavior() )
            + ", Name:     " + m_levelObjects[i]->GetFriendlyName()
            + ", X, Y:     " + StringUtil::FloatToString( m_levelObjects[i]->GetPosition().x ) + ", "
                             + StringUtil::FloatToString( m_levelObjects[i]->GetPosition().y )
            + ", Dimension:" + StringUtil::FloatToString( m_levelObjects[i]->GetPosition().w ) + " x "
                             + StringUtil::FloatToString( m_levelObjects[i]->GetPosition().h ),
            "Level::TileDebugOut"
         );
    }
}

float Level::GetDistance( const kuko::FloatRect& a, const kuko::FloatRect& b )
{
    float centerX1 = a.x + a.w/2;
    float centerX2 = b.x + a.w/2;
    float centerY1 = a.y + a.h/2;
    float centerY2 = b.y + a.h/2;

    float diffX = centerX1 - centerX2;
    float diffY = centerY1 - centerY2;
    return sqrt( diffX * diffX + diffY * diffY );
}

void Level::IsInEditor( bool value )
{
    m_editorState = value;
}
