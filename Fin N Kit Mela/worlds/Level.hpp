#ifndef LEVEL_HPP
#define LEVEL_HPP

#include "../entities/Object.hpp"

#include <vector>
#include <string>

class Level
{
    public:
    Level();
    ~Level();

    void Begin();
    void SetPath( const std::string& path );
    void SetTitle( const std::string& title );
    unsigned int GetTileCount();
    int AddTile( Object* obj );
    void RemoveTile( int index );
    Object& GetTile( int index );
    Object* GetTilePtr( int index );
    ItemCategory Collision( const kuko::BaseEntity& entity );
    int GetRemainingTrinketCount();
    void Draw( float scrollOffset = 0, float scale = 1 );
    void Draw( float x, float y, float scale );
    void DrawForeground( float scrollOffset = 0, float scale = 1 );
    void DrawForeground( float x, float y, float scale );

    void Clear();

    void Scroll( float amount );
    void EditorScroll( float amount );
    void SetScroll( float amount );
    void Update();

    void Save( const std::string& filename );
    void Load( const std::string& filename );

    void AddBackgroundTexture( SDL_Texture* texture );
    void SetGroundTextures( SDL_Texture* txtBackground, SDL_Texture* txtForeground );
    void ScrollBackground( float amount );

    void SetBackground( int val );
    void SetGround( int val );
    void SetSong( int val );
    int GetSongID();
    void UpdateProperties();
    void SetScrollSpeed( float val );
    float GetScrollSpeed();
    float GetDistance( const kuko::FloatRect& a, const kuko::FloatRect& b );
    void IsInEditor( bool value );

    void TileDebugOut();

    private:
    kuko::Sprite m_backgrounds[2];
    kuko::Sprite m_grounds[4];
    std::vector<SDL_Texture*> m_backgroundTextures;
    std::vector<Object*> m_levelObjects;
    std::string m_levelPath;
    std::string m_title;
    float m_offset;
    int m_groundId;
    int m_backgroundId;
    int m_songId;
    std::string m_song;
    float m_scrollSpeed;
    bool m_editorState;
};

#endif
