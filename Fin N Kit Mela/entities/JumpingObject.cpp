#include "JumpingObject.hpp"

#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"

JumpingObject::JumpingObject()
    : kuko::BaseEntity()
{
    Reset();
}

void JumpingObject::Reset()
{
    m_jumpCounter = 0;
    m_canDoubleJump = false;
    m_jumpCount = 0;
    m_launchCounter = 0;
    m_position = { 0, 0, 0, 0 };
    m_velocityY = 0;

    m_gravity = 0.1;
    // Velocity measured in pixels per second
    m_initialVelocity = -640;

    m_maxY = kuko::Application::GetDefaultHeight();
}

void JumpingObject::SetName( const std::string& name )
{
    m_name = name;
}

void JumpingObject::BeginJump()
{
    if ( m_jumpCount == 0 )
    {
        // 1st jump
        m_velocityY = m_initialVelocity;
        m_jumpCount = 1;

        if ( m_name == "Fin" )
        {
            kuko::SoundManager::PlaySound( "jump_fin" );
        }
        else if ( m_name == "Kit" )
        {
            kuko::SoundManager::PlaySound( "jump_kit" );
        }
    }
    else if ( m_canDoubleJump && m_jumpCount == 1 )
    {
        // 2nd jump
        m_velocityY = m_initialVelocity;
        m_jumpCount = 2;
        kuko::SoundManager::PlaySound( "jump_kit" );
    }
}

void JumpingObject::SetPhysics( float gravity, float initVelocity, bool canDoubleJump, float maxY )
{
    m_gravity = gravity;
    m_initialVelocity = initVelocity;
    m_canDoubleJump = canDoubleJump;
    m_maxY = maxY;
}

void JumpingObject::SetDoubleJumpAbility( bool value )
{
    m_canDoubleJump = value;
}

void JumpingObject::Fall( float timeStep )
{
    m_velocityY += m_gravity * timeStep;
}

void JumpingObject::HitGround()
{
    m_velocityY = 0;
    m_jumpCount = 0;
}

void JumpingObject::Launch()
{
    m_launchCounter = 10;
    m_sprite.SetFrame( m_jumpFrame );
}

void JumpingObject::SetJumpFrame( const kuko::IntRect& rect )
{
    m_jumpFrame = rect;
}

void JumpingObject::SetDefaultFrame( const kuko::IntRect& rect )
{
    m_defaultFrame = rect;
}

void JumpingObject::Update()
{
    kuko::BaseEntity::Update();
}

void JumpingObject::Update( float timeStep )
{
    m_position.y += m_velocityY * timeStep;
    if ( m_position.y >= m_maxY )
    {
        m_position.y = m_maxY;
        HitGround();
    }
    else if ( m_position.y < -100 )
    {
        m_position.y = -100;
    }
    else
    {
        Fall( timeStep );
    }

    if ( IsJumping() )
    {
        m_sprite.SetFrame( m_jumpFrame );
    }
    else if ( m_launchCounter <= 0 )
    {
        m_sprite.SetFrame( m_defaultFrame );
    }
    else if ( m_launchCounter > 0 )
    {
        m_launchCounter -= 0.1;
    }

    kuko::BaseEntity::Update();
}

bool JumpingObject::IsJumping()
{
    return ( m_jumpCount > 0 );
}

void JumpingObject::PushUp( const kuko::BaseEntity& pusher )
{
    m_position.y = pusher.GetPosition().y - m_collisionRegion.h;
}

void JumpingObject::Draw()
{
    kuko::BaseEntity::Draw();
    // Draw collision rect
//    kuko::FloatRect collision = m_collisionRegion;
//    collision.x += m_position.x;
//    collision.y += m_position.y;
//    kuko::ImageManager::DrawRectangle( collision, 255, 0, 0, 1 );
}
