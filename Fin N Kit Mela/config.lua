config = {
	borderless = "false",
	fullscreen = "true",
	language = "english",
	music_volume = "0",
	savegame_count = "",
	screen_height = "720",
	screen_width = "1280",
	sound_volume = "1",
	vsync = "true",
}
