#ifndef LEVEL_EDITOR_STATE_HPP
#define LEVEL_EDITOR_STATE_HPP

#include "../Kuko/states/IState.hpp"
#include "../Kuko/managers/MenuManager.hpp"
#include "../worlds/Level.hpp"

#include <vector>
#include <string>

struct CurrentBrush
{
    CurrentBrush() { name = ""; }
    std::string name;
    std::string friendlyName;
    kuko::Sprite sprite;
    kuko::FloatRect buttonPos;
};

class LevelEditorState : public kuko::IState
{
    public:
    LevelEditorState( const std::string& contentPath );
    virtual ~LevelEditorState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    private:
    kuko::MenuManager menuManager;

    void SetupMenu();
    void SetupGrid();

    void HandleBrushUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );
    void HandlePageChanges( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );
    void HandleTextEntry( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );
    void HandleTilePlacement( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );
    void HandleBehaviorUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );
    void HandleScaleUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );
    void HandleLevelPropertiesUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );
    void HandleFileButtons( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );

    void UpdateCurrentBrush();
    void UpdateScroll();

    void UpdateBehaviorButtons();
    void UpdateScaleButtons();

    void PlaceTile( float x, float y );
    void EraseTile( float x, float y );
    void SelectActiveItem( int index );

    void UpdateLevelFile( const std::string& newMap );

    void UpdateStatus( const std::string& status="" );

    std::string m_state;
    std::string m_contentPath;

    std::vector<kuko::Sprite*> m_grid;
    std::vector<kuko::UILabel*> m_gridLabels;
    kuko::UILabel m_statusBar;
    float m_statusBarTimer;
    int m_scrollOffset;
    kuko::FloatRect m_defaultSize;

    CurrentBrush m_brush;

    bool m_erasing;
    Level m_level;

    int m_activeItem;
    Behavior m_defaultBehavior;
    float m_defaultScale;
    std::vector<std::string> m_behaviorList;
};

#endif
