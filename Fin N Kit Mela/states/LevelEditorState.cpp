#include "LevelEditorState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/managers/ConfigManager.hpp"
#include "../Kuko/utilities/Logger.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"

#include "../utils/Messager.hpp"

#include <fstream>

LevelEditorState::~LevelEditorState()
{
    Cleanup();
}

LevelEditorState::LevelEditorState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void LevelEditorState::Setup()
{
    Logger::Out( "Setup", "LevelEditorState" );
    kuko::IState::Setup();
    m_scrollOffset = 0;
    m_erasing = false;
    m_level.SetPath( "levels/ ");
    SetupMenu();
    SetupGrid();

    kuko::ImageManager::AddTexture( "background_sand", "content/graphics/game/background_sand.png" );
    kuko::ImageManager::AddTexture( "behavior_icons", "content/graphics/ui/editor/behavior_tiny.png" );

    if ( Messager::msg["level"] != "" )
    {
        m_level.Load( Messager::msg["level"] );
        m_level.Draw();

        float speed = m_level.GetScrollSpeed();
        if ( speed == 0.25 ) { menuManager.GetButton( "btn_speed1" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) ); }
        else if ( speed == 0.50 ) { menuManager.GetButton( "btn_speed2" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) ); }
        else if ( speed == 0.75 ) { menuManager.GetButton( "btn_speed3" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) ); }
        else { menuManager.GetButton( "btn_speed4" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) ); }

        int song = m_level.GetSongID();
        if ( song == 1 ) { kuko::SoundManager::PlayMusic( "level1", true ); }
        else if ( song == 2 ) { kuko::SoundManager::PlayMusic( "level2", true ); }
        else { kuko::SoundManager::PlayMusic( "level3", true ); }

        std::string withoutExtension = Messager::msg["level"];
        int extentionPos = Messager::msg["level"].find( ".fin" );
        withoutExtension = withoutExtension.substr( 0, extentionPos );

        menuManager.SetTextboxValue( "property_mapname", withoutExtension );
        Messager::msg["level"] = "";
    }
    else
    {
        m_level.SetScrollSpeed( 0.25 );
        menuManager.GetButton( "btn_speed1" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) );
        kuko::SoundManager::PlayMusic( "level1", true );
        m_level.SetSong( 1 );
    }
    m_level.AddBackgroundTexture( kuko::ImageManager::GetTexture( "background_sand" ) );
    m_level.SetGroundTextures( kuko::ImageManager::GetTexture( "background_waves" ), kuko::ImageManager::GetTexture( "foreground_waves" ) );
    m_level.Begin();
    m_level.IsInEditor( true );

    std::vector<std::string> behaviors = { "behavior_still", "behavior_down", "behavior_jump",
        "behavior_left", "behavior_right", "behavior_up",
        "behavior_sine_left", "behavior_sine_right", "behavior_counter_clockwise", "behavior_clockwise",
        "behavior_updown", "behavior_leftright" };
    m_behaviorList = behaviors;

    m_activeItem = -1;
    m_defaultBehavior = STILL;
    m_defaultScale = 1.0;
    UpdateBehaviorButtons();
    UpdateScaleButtons();

    UpdateScroll();

    Draw();
}

void LevelEditorState::Cleanup()
{
    Logger::Out( "Cleanup", "LevelEditorState::Cleanup" );
    kuko::ImageManager::ClearTextures();

    for ( unsigned int i = 0; i < m_gridLabels.size(); i++ )
    {
        if ( m_gridLabels[i] != NULL )
        {
            delete m_gridLabels[i];
        }
    }

    for ( unsigned int i = 0; i < m_grid.size(); i++ )
    {
        if ( m_grid[i] != NULL )
        {
            delete m_grid[i];
        }
    }

    menuManager.ClearMenu();

    m_grid.clear();
    m_gridLabels.clear();
    m_level.Clear();
}

void LevelEditorState::SetContentPath( const std::string& path )
{
    Logger::Out( "", "LevelEditorState::SetContentPath" );
    m_contentPath = path;
}

void LevelEditorState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();
    std::string keyHit = kuko::InputManager::GetKeyHit();

    bool leftTap = input[ kuko::TAP ].down;
    int leftX = input[ kuko::TAP ].actionX;
    int leftY = input[ kuko::TAP ].actionY;
    std::string clickedButtonId = menuManager.GetButtonClicked( leftX, leftY );

    if ( input[ kuko::MOVE_LEFT ].down || ( leftTap && clickedButtonId == "btn_left" ) )
    {
        Logger::Out( "Move Left", "LevelEditorState::Update" );
        m_scrollOffset -= 1;
        m_level.EditorScroll( 1 * 80 );
        UpdateScroll();
    }

    else if ( input[ kuko::MOVE_RIGHT ].down || ( leftTap && clickedButtonId == "btn_right" ) )
    {
        Logger::Out( "Move Right", "LevelEditorState::Update" );
        m_scrollOffset += 1;
        m_level.EditorScroll( -1 * 80 );
        UpdateScroll();
    }

    else if ( clickedButtonId == "button_exit" )
    {
        Logger::Out( "Move Exit", "LevelEditorState::Update" );
        m_gotoState = "title";
    }

    else if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        Logger::Out( "Move Quit", "LevelEditorState::Update" );
        m_gotoState = "quit";
    }

    else
    {

        HandleTextEntry( input );
        HandlePageChanges( input );
        HandleBrushUpdate( input );
        HandleTilePlacement( input );
        HandleBehaviorUpdate( input );
        HandleScaleUpdate( input );
        HandleLevelPropertiesUpdate( input );
        HandleFileButtons( input );

    }

    menuManager.Update();
    UpdateStatus();
}

void LevelEditorState::HandleBrushUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    std::string clickedButtonId = "";
    int x=-1, y=-1;

    if ( input[ kuko::TAP ].down )
    {
        x = input[ kuko::TAP ].actionX;
        y = input[ kuko::TAP ].actionY;
        clickedButtonId = menuManager.GetButtonClicked( x, y );
    }
    else
    {
        return;
    }

    bool updateBrush = true;
    if ( clickedButtonId.find( "btn_obstacle" ) != std::string::npos )
    {
        m_brush.name = clickedButtonId.substr( 4, clickedButtonId.size() );
    }
    else if ( clickedButtonId.find( "btn_trinket" ) != std::string::npos )
    {
        m_brush.name = clickedButtonId.substr( 4, clickedButtonId.size() );
    }
    else if ( clickedButtonId == "btn_extra_heart" )
    {
        m_brush.name = "extra_heart";
    }
    else if ( clickedButtonId == "btn_end_of_level" )
    {
        m_brush.name = "end_of_level";
    }
    else
    {
        updateBrush = false;
    }

    m_brush.friendlyName = Object::GetFriendlyName( m_brush.name );
    UpdateStatus( kuko::LanguageManager::Text( "editor_status_current_brush" ) + m_brush.friendlyName );

    if ( updateBrush )
    {
        kuko::UIButton* button = NULL;
        button = menuManager.GetButton( clickedButtonId );

        if ( button != NULL )
        {
            m_brush.buttonPos = button->GetPosition();
        }
    }

    UpdateCurrentBrush();
}

void LevelEditorState::HandleTilePlacement( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    int x, y;

    if ( input[ kuko::TAP ].down )
    {
        x = input[ kuko::TAP ].actionX;
        y = input[ kuko::TAP ].actionY;

        int toolbarX = ( kuko::Application::GetDefaultWidth() - 260 ) * kuko::Application::GetWidthRatio();

        Logger::Debug( "DEBUG Toolbar X: " + StringUtil::IntToString( toolbarX ) );
        Logger::Debug( "DEBUG Width Ratio: " + StringUtil::FloatToString( kuko::Application::GetWidthRatio() ) );

        if ( x < toolbarX || menuManager.GetCurrentPage() == 5 )
        {
            if ( m_erasing )
            {
                Logger::Out( "Erase", "LevelEditorState::HandleTilePlacement" );
                EraseTile( x, y );
            }
            else
            {
                Logger::Out( "Place", "LevelEditorState::HandleTilePlacement" );
                PlaceTile( x, y );
            }
        }
    }
    else if ( input[ kuko::SECONDARY_TAP ].down )
    {
        x = input[ kuko::SECONDARY_TAP ].actionX;
        y = input[ kuko::SECONDARY_TAP ].actionY;

        Logger::Out( "Erase", "LevelEditorState::HandleTilePlacement" );
        EraseTile( x, y );
    }
}

void LevelEditorState::HandleFileButtons( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX;
        int y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        if ( clickedButtonId == "btn_save_map" )
        {
            std::string path = menuManager.GetTextboxValue( "property_mapname" );
            path = ( path == "" ) ? "default.fin" : path + ".fin";
            m_level.SetTitle( menuManager.GetTextboxValue( "property_mapname" ) );
            m_level.Save( path );

            UpdateStatus( kuko::LanguageManager::Text( "editor_saved" ) + path );
            UpdateLevelFile( path );
        }
        else if ( clickedButtonId == "btn_load_map" )
        {
            std::string path = menuManager.GetTextboxValue( "property_mapname" );
            path = ( path == "" ) ? "default.fin" : path + ".fin";
            m_level.Load( path );
            m_level.Begin();
        }
        else if ( clickedButtonId == "btn_test_map" )
        {
            m_level.Save( "test.fin" );
            Messager::msg["level"] = "test.fin";
            Messager::msg["previousState"] = "level_editor";
            m_gotoState = "juggle_game";
        }
        else if ( clickedButtonId == "btn_clear_map" )
        {
            m_level.Clear();
            m_activeItem = -1;
        }
    }
}

void LevelEditorState::HandleLevelPropertiesUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    std::string clickedButtonId = "";
    int x=-1, y=-1;

    if ( input[ kuko::TAP ].down )
    {
        x = input[ kuko::TAP ].actionX;
        y = input[ kuko::TAP ].actionY;
        clickedButtonId = menuManager.GetButtonClicked( x, y );

        // Backgrounds
        if ( clickedButtonId == "background1" )
        {
            m_level.AddBackgroundTexture( kuko::ImageManager::GetTexture( "background_sand" ) );
            m_level.SetBackground( 1 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_background" ) );
        }
        else if ( clickedButtonId == "background2" )
        {
            m_level.AddBackgroundTexture( kuko::ImageManager::GetTexture( "background_tank" ) );
            m_level.SetBackground( 2 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_background" ) );
        }
        else if ( clickedButtonId == "background3" )
        {
            m_level.AddBackgroundTexture( kuko::ImageManager::GetTexture( "background_night" ) );
            m_level.SetBackground( 3 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_background" ) );
        }

        // Grounds
        if ( clickedButtonId == "ground1" )
        {
            m_level.SetGroundTextures(
                kuko::ImageManager::GetTexture( "background_waves" ),
                kuko::ImageManager::GetTexture( "foreground_waves" ) );
            m_level.SetGround( 1 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_ground" ) );
        }
        else if ( clickedButtonId == "ground2" )
        {
            m_level.SetGroundTextures(
                kuko::ImageManager::GetTexture( "background_waves2" ),
                kuko::ImageManager::GetTexture( "foreground_waves2" ) );
            m_level.SetGround( 2 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_ground" ) );
        }
        else if ( clickedButtonId == "ground3" )
        {
            m_level.SetGroundTextures(
                kuko::ImageManager::GetTexture( "background_waves3" ),
                kuko::ImageManager::GetTexture( "foreground_waves3" ) );
            m_level.SetGround( 3 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_ground" ) );
        }

        // Songs
        if ( clickedButtonId == "song1" )
        {
            kuko::SoundManager::PlayMusic( "level1", true );
            m_level.SetSong( 1 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_song" ) );
        }
        else if ( clickedButtonId == "song2" )
        {
            kuko::SoundManager::PlayMusic( "level2", true );
            m_level.SetSong( 2 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_song" ) );
        }
        else if ( clickedButtonId == "song3" )
        {
            kuko::SoundManager::PlayMusic( "level3", true );
            m_level.SetSong( 3 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_song" ) );
        }

        // speeds
        if ( clickedButtonId == "btn_speed1" )
        {
            m_level.SetScrollSpeed( 0.25 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_speed" ) );
            // Update these buttons
            menuManager.GetButton( "btn_speed1" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) );
            menuManager.GetButton( "btn_speed2" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed3" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed4" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
        }
        else if ( clickedButtonId == "btn_speed2" )
        {
            m_level.SetScrollSpeed( 0.50 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_speed" ) );
            // Update these buttons
            menuManager.GetButton( "btn_speed1" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed2" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) );
            menuManager.GetButton( "btn_speed3" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed4" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
        }
        else if ( clickedButtonId == "btn_speed3" )
        {
            m_level.SetScrollSpeed( 0.75 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_speed" ) );
            // Update these buttons
            menuManager.GetButton( "btn_speed1" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed2" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed3" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) );
            menuManager.GetButton( "btn_speed4" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
        }
        else if ( clickedButtonId == "btn_speed4" )
        {
            m_level.SetScrollSpeed( 1.00 );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_updated_speed" ) );
            // Update these buttons
            menuManager.GetButton( "btn_speed1" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed2" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed3" )->SetFrame( kuko::IntRect( 0, 0, 40, 40 ) );
            menuManager.GetButton( "btn_speed4" )->SetFrame( kuko::IntRect( 0, 40, 40, 40 ) );
        }
    }

}

void LevelEditorState::HandlePageChanges( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    std::string keyHit = kuko::InputManager::GetKeyHit();
    std::string clickedButtonId = "";
    int x, y;

    if ( input[ kuko::TAP ].down )
    {
        x = input[ kuko::TAP ].actionX;
        y = input[ kuko::TAP ].actionY;
        clickedButtonId = menuManager.GetButtonClicked( x, y );
    }

    if ( keyHit == "1" || clickedButtonId.find( "goto1" ) != std::string::npos )
    {
        menuManager.SetCurrentPage( 1 );
    }
    else if ( keyHit == "2" || clickedButtonId.find( "goto2" ) != std::string::npos )
    {
        menuManager.SetCurrentPage( 2 );
    }
    else if ( keyHit == "3" || clickedButtonId.find( "goto3" ) != std::string::npos )
    {
        menuManager.SetCurrentPage( 3 );
    }
    else if ( keyHit == "4" || clickedButtonId.find( "goto4" ) != std::string::npos )
    {
        menuManager.SetCurrentPage( 4 );
    }
    else if ( keyHit == "5" || clickedButtonId.find( "minimize_editor" ) != std::string::npos )
    {
        menuManager.SetCurrentPage( 5 );
    }
    else if ( clickedButtonId.find( "maximize_editor" ) != std::string::npos )
    {
        menuManager.SetCurrentPage( 1 );
    }
}

void LevelEditorState::HandleTextEntry( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    if ( input[ kuko::BACKSPACE ].down )
    {
        menuManager.RemoveLastCharacterOfActiveTextBox();
    }

    if ( kuko::InputManager::GetTextInputBuffer() != "" )
    {
        menuManager.AppendToActiveTextBox( kuko::InputManager::GetTextInputBuffer() );
    }

    if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX;
        int y = input[ kuko::TAP ].actionY;

        if ( menuManager.CheckTextboxClick( x, y ) )
        {
            // Textbox
            menuManager.SetTextEditing( true );
            kuko::InputManager::SetTextBufferActive( true );
        }
    }
}

void LevelEditorState::HandleBehaviorUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX;
        int y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        int behaviorId = -1;
        for ( unsigned int i = 0; i < m_behaviorList.size(); i++)
        {
            if ( m_behaviorList[i] == clickedButtonId )
            {
                behaviorId = i;
                break;
            }
        }

        if ( behaviorId == -1 )
        {
            return;
        }

        // If nothing is selected, choose the default behavior for future tiles.
        if ( m_activeItem == -1 )
        {
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_set_default_behavior" ) );
            m_defaultBehavior = Behavior( behaviorId );
        }

        // If something is selected, set its behavior.
        else
        {
            Object& obj = m_level.GetTile( m_activeItem );
            obj.SetBehavior( Behavior( behaviorId ) );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_set_tile_behavior" )
                + Object::GetFriendlyBehavior( Behavior( behaviorId ) ) );
        }

        UpdateBehaviorButtons();
    }
}

void LevelEditorState::HandleScaleUpdate( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    if ( input[ kuko::TAP ].down )
    {
        Logger::Out( "Update", "LevelEditorState::HandleScaleUpdate" );

        int x = input[ kuko::TAP ].actionX;
        int y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        float scale = m_defaultScale;

        if ( clickedButtonId != "scale_50" && clickedButtonId != "scale_100" && clickedButtonId != "scale_150" )
        {
            return;
        }
        else if ( clickedButtonId == "scale_50" ) { scale = 0.5; }
        else if ( clickedButtonId == "scale_100" ) { scale = 1.0; }
        else if ( clickedButtonId == "scale_150" ) { scale = 1.5; }

        // If nothing is selected, choose the default behavior for future tiles.
        if ( m_activeItem == -1 )
        {
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_set_default_scale" ) );
            m_defaultScale = scale;
        }

        // If something is selected, set its behavior.
        else
        {
            Object& obj = m_level.GetTile( m_activeItem );
            obj.SetScale( scale );
            UpdateStatus( kuko::LanguageManager::Text( "editor_status_set_tile_scale" )
                + StringUtil::FloatToString( scale * 100 ) + "%" );
        }

        UpdateScaleButtons();
    }
}

void LevelEditorState::UpdateScroll()
{
    // Update grid labels
    for ( unsigned int i = 0; i < m_gridLabels.size(); i++ )
    {
        std::string index = StringUtil::IntToString( i + m_scrollOffset );
        m_gridLabels[i]->ChangeText( index );
    }
}

void LevelEditorState::UpdateLevelFile( const std::string& newMap )
{
    std::vector<std::string> levels;

    std::ifstream input( "maps.txt" );
    std::string level;
    while ( input >> level )
    {
        if ( level != newMap )
        {
            levels.push_back( level );
        }
    }
    input.close();

    levels.push_back( newMap );

    std::ofstream output( "maps.txt" );
    for ( unsigned int i = 0; i < levels.size(); i++ )
    {
        output << levels[i] << std::endl;
    }
    output.close();
}

void LevelEditorState::UpdateCurrentBrush()
{
}

void LevelEditorState::PlaceTile( float x, float y )
{
    x /= kuko::Application::GetWidthRatio();
    y /= kuko::Application::GetHeightRatio();

    for ( unsigned int i = 0; i < m_grid.size(); i++ )
    {
        kuko::FloatRect gridPos = m_grid[i]->GetPosition();

        // Clicking this grid item
        if ( x >= gridPos.x && x <= gridPos.x + gridPos.w &&
             y >= gridPos.y && y <= gridPos.y + gridPos.h )
        {
            // Is there an item already within this grid item?
            for ( unsigned int j = 0; j < m_level.GetTileCount(); j++ )
            {
                // The tile can occupy multiple grid-blocks; maybe check
                // to see if it is centered here.
                kuko::FloatRect tilePos = m_level.GetTile( j ).GetPosition();
                int centerX = tilePos.x + tilePos.w/2;
                int centerY = tilePos.y + tilePos.h/2;

                if ( gridPos.x < centerX && centerX < gridPos.x + gridPos.w
                    && gridPos.y < centerY && centerY < gridPos.y + gridPos.h )
                {
                    Logger::Out( "Tile match, select tile", "LevelEditorState::PlaceTile" );
                    // Select this tile instead
                    SelectActiveItem( j );
                    return;
                }
            }

            // No tile here found. Create new one.
            if ( m_brush.name == "" ) { return; }
            std::string name = "tile_" + StringUtil::IntToString( m_level.GetTileCount() );

            Object* obj = new Object;
            obj->Setup( name, kuko::ImageManager::GetTexture( m_brush.name ), gridPos );
            obj->SetType( m_brush.name );
            obj->SetDebugFrame( false );
            obj->SetScale( m_defaultScale );
            obj->SetBehavior( Behavior( m_defaultBehavior ) );

            kuko::FloatRect actualPosition = obj->GetPosition();
            actualPosition.x += m_scrollOffset * 80;
            obj->SetDefaultPosition( actualPosition );
            obj->SetIsInEditor( true );

            UpdateStatus( kuko::LanguageManager::Text( "editor_status_added_new_tile" )
                + obj->GetFriendlyName() );

            int newIndex = m_level.AddTile( obj );

            if ( m_activeItem != -1 )
            {
                m_level.GetTile( m_activeItem ).SetSelected( false );
            }
            m_activeItem = newIndex;
            UpdateBehaviorButtons();
            break;
        }
    }
}

void LevelEditorState::SelectActiveItem( int index )
{
    m_activeItem = index;
    UpdateBehaviorButtons();
    UpdateScaleButtons();
    m_level.GetTile( m_activeItem ).SetIsInEditor( true );
}

void LevelEditorState::UpdateBehaviorButtons()
{
    // Reset buttons
    for ( int i = 0; i < 12; i++ )
    {
        kuko::UIButton* ptrButton = menuManager.GetButton( m_behaviorList[i] );
        kuko::IntRect frame = ptrButton->GetFrame();
        frame.y = 0;
        ptrButton->SetFrame( frame );
    }

    if ( m_activeItem != -1 )
    {
        Behavior behavior = m_level.GetTile( m_activeItem ).GetBehavior();
        Logger::Out( "Active item behavior: " + StringUtil::IntToString( behavior ), "LevelEditorState::UpdateBehaviorButtons" );

        // Update each behavior button
        for ( int i = 0; i < 12; i++ )
        {
            if ( i == behavior )
            {
                kuko::UIButton* ptrButton = menuManager.GetButton( m_behaviorList[i] );
                kuko::IntRect frame = ptrButton->GetFrame();
                frame.y = 40;
                ptrButton->SetFrame( frame );
            }
        }
    }
    else
    {
        Logger::Out( "Set default behavior" );
        // Check default button highlight
        kuko::UIButton* ptrButton = menuManager.GetButton( m_behaviorList[ m_defaultBehavior ] );
        kuko::IntRect frame = ptrButton->GetFrame();
        frame.y = 40;
        ptrButton->SetFrame( frame );
    }
}

void LevelEditorState::UpdateScaleButtons()
{
    // Reset button colors
    kuko::IntRect frame50 = menuManager.GetButton( "scale_50" )->GetFrame();
    kuko::IntRect frame100 = menuManager.GetButton( "scale_100" )->GetFrame();
    kuko::IntRect frame150 = menuManager.GetButton( "scale_150" )->GetFrame();

    float scale = m_defaultScale;
    if ( m_activeItem != -1 )
    {
        scale = m_level.GetTile( m_activeItem ).GetScale();
    }

    if ( scale == .5 )
    {
        scale = 0.5;
        frame50.y = 40;
    }
    else
    {
        frame50.y = 0;
    }

    if ( scale == 1 )
    {
        scale = 1.0;
        frame100.y = 40;
    }
    else
    {
        frame100.y = 0;
    }

    if ( scale == 1.5 )
    {
        scale = 1.5;
        frame150.y = 40;
    }
    else
    {
        frame150.y = 0;
    }

    menuManager.GetButton( "scale_50" )->SetFrame( frame50 );
    menuManager.GetButton( "scale_100" )->SetFrame( frame100 );
    menuManager.GetButton( "scale_150" )->SetFrame( frame150 );
}

void LevelEditorState::EraseTile( float x, float y )
{
    int removeIndex = -1;
    Object* ptrActiveItem = NULL;
    if ( m_activeItem != -1 )
    {
        ptrActiveItem = &m_level.GetTile( m_activeItem );
    }

    // Right-clicking on grid
    for ( unsigned int i = 0; i < m_level.GetTileCount(); i++ )
    {
        kuko::FloatRect pos = m_level.GetTile( i ).GetPosition();

        if ( x >= pos.x && x <= pos.x + pos.w &&
             y >= pos.y && y <= pos.y + pos.h )
        {
            // Remove this tile
            removeIndex = i;
            break;
        }
    }

    if ( removeIndex != -1 )
    {
        // Remove item
        m_level.RemoveTile( removeIndex );

        if ( m_activeItem == removeIndex )
        {
            m_activeItem = -1;
        }
        else if ( m_activeItem > removeIndex )
        {
            m_activeItem -= 1;
        }
        UpdateBehaviorButtons();
        UpdateStatus( kuko::LanguageManager::Text( "editor_status_removed_tile" ) );
    }
}

void LevelEditorState::Draw()
{
    m_level.Draw();
    m_level.DrawForeground();

    for ( unsigned int i = 0; i < m_grid.size(); i++ )
    {
        m_grid[i]->Draw();
    }
    for ( unsigned int i = 0; i < m_gridLabels.size(); i++ )
    {
        m_gridLabels[i]->Draw();
    }
    if ( m_activeItem != -1 )
    {
        // Add box around selected item
        Object& obj = m_level.GetTile( m_activeItem );
        kuko::ImageManager::DrawRectangle( obj.GetPosition(), 0x00, 0xff, 0xff, 5 );
    }

    menuManager.Draw();

    if ( menuManager.GetCurrentPage() == 1 && m_brush.name != "" )
    {
        // Add box around selected brush
        kuko::ImageManager::DrawRectangle( m_brush.buttonPos, 0xff, 0xff, 0x00 );
    }
    if ( menuManager.GetCurrentPage() != 5 )
    {
        // Don't draw Mini-map when collapsed
//        m_level.Draw( 1024, 544, 0.20 );
        // Don't draw status bar when collapsed
        m_statusBar.Draw();
    }

    m_brush.sprite.Draw();
}

void LevelEditorState::SetupMenu()
{
    m_state = "level_editor";
    menuManager.LoadMenuScript( m_contentPath + "menus/level_editor.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();
    menuManager.SetCurrentPage( 1 );

    // Status bar

    m_statusBar.SetFont( kuko::FontManager::GetFont( "main" ) );
    m_statusBar.SetColor( 255, 255, 255, 255 );
    m_statusBar.SetCentered( false );
    m_statusBar.SetPosition( kuko::FloatRect( 1028, 693, 80, 25 ) );
    m_statusBar.ChangeText( kuko::LanguageManager::Text( "editor_status_hello" ) );
}

void LevelEditorState::SetupGrid()
{
    kuko::ImageManager::AddTexture( "grid", m_contentPath + "graphics/ui/editor/btn_music.png" );
    kuko::ImageManager::AddTexture( "bg", m_contentPath + "graphics/game/background_sky.png" );
    kuko::ImageManager::AddTexture( "ground", m_contentPath + "graphics/game/waves_still.png" );

    int width = kuko::Application::GetDefaultWidth();
    int height = kuko::Application::GetDefaultHeight();

    int horizCount = kuko::Application::GetDefaultWidth() / 80;
    int vertCount = kuko::Application::GetDefaultHeight() / 80;

    for ( int y = 0; y < vertCount; y++ )
    {
        for ( int x = 0; x < horizCount; x++ )
        {
            kuko::FloatRect pos;
            pos.x = x * 80;
            pos.y = y * 80;
            pos.w = 80;
            pos.h = 80;
            // Create grid image
            kuko::Sprite* grid = new kuko::Sprite;
            grid->SetTexture( kuko::ImageManager::GetTexture( "grid" ) );
            grid->SetPosition( pos );
            m_grid.push_back( grid );

            if ( y == 0 )
            {
                kuko::UILabel* lbl = new kuko::UILabel;
                lbl->SetFont( kuko::FontManager::GetFont( "main" ) );
                lbl->SetColor( 255, 255, 255, 255 );
                lbl->SetCentered( false );
                lbl->SetPosition( kuko::FloatRect( x * 80+2, 2, 80, 30 ) );
                lbl->ChangeText( StringUtil::IntToString( x ) );
                m_gridLabels.push_back( lbl );
            }
        }
    }
}

void LevelEditorState::UpdateStatus( const std::string& status )
{
    if ( status == "" && m_statusBarTimer > 0 )
    {
        m_statusBarTimer -= 1;
    }
    else if ( status != "" )
    {
        Logger::Out( status, "LevelEditorState::UpdateStatus" );
        m_statusBar.ChangeText( status );
        m_statusBarTimer = 500;
    }

    if ( m_statusBarTimer == 0 )
    {
        // Default message: Tile count
        m_statusBar.ChangeText( kuko::LanguageManager::Text( "editor_status_tile_count" )
            + StringUtil::IntToString( m_level.GetTileCount() ) );

        m_statusBarTimer = -1;
    }
}


