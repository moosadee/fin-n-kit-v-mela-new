#ifndef _CustomLevelLoaderState
#define _CustomLevelLoaderState

#include "../Kuko/states/IState.hpp"
#include "../Kuko/managers/MenuManager.hpp"
#include "../Kuko/widgets/UIButton.hpp"
#include "../Kuko/widgets/UILabel.hpp"

#include <vector>
#include <string>

class CustomLevelLoaderState : public kuko::IState
{
    public:
    CustomLevelLoaderState( const std::string& contentPath );
    virtual ~CustomLevelLoaderState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    private:
    kuko::MenuManager menuManager;

    void SetupMenu();
    void SetupLevelButtons();

    std::string m_state;
    std::vector< std::string > m_lstSaveGames;

    std::string m_contentPath;
};

#endif

