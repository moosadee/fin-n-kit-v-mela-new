#include "JuggleGameState.hpp"

#include <cstdlib>

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/utilities/StringUtil.hpp"
#include "../utils/Messager.hpp"

JuggleGameState::JuggleGameState( const std::string& contentPath ) : IState()
{
    m_contentPath = contentPath;
}

void JuggleGameState::Setup()
{
    Logger::Out( "Setup", "JuggleGameState::Setup" );
    kuko::IState::Setup();

    int screenWidth = kuko::Application::GetDefaultWidth();
    int screenHeight = kuko::Application::GetDefaultHeight();

    kuko::ImageManager::AddTexture( "bg", "content/graphics/game/background_sky.png" );
    kuko::ImageManager::AddTexture( "waves", "content/graphics/game/waves.png" );
    kuko::ImageManager::AddTexture( "sand1", "content/graphics/game/background_sand.png" );
    kuko::ImageManager::AddTexture( "sand2", "content/graphics/game/background_sand2.png" );
    kuko::ImageManager::AddTexture( "sand3", "content/graphics/game/background_sand3.png" );

    kuko::ImageManager::AddTexture( "fin", "content/graphics/game/fin.png" );
    kuko::ImageManager::AddTexture( "kit", "content/graphics/game/kit.png" );
    kuko::ImageManager::AddTexture( "heart", "content/graphics/game/extra_heart.png" );
    kuko::ImageManager::AddTexture( "score_icons", "content/graphics/ui/star_score.png" );

    kuko::ImageManager::AddTexture( "btn_pause", "content/graphics/ui/btn_pause.png" );
    kuko::ImageManager::AddTexture( "shade_black", "content/graphics/ui/shade_black.png" );
    kuko::ImageManager::AddTexture( "bg_paused", "content/graphics/ui/background_paused.png" );
    kuko::ImageManager::AddTexture( "btn_continue_level", "content/graphics/ui/btn_continue_level.png" );
    kuko::ImageManager::AddTexture( "btn_exit_level", "content/graphics/ui/btn_exit_level.png" );
    kuko::ImageManager::AddTexture( "btn_restart_level", "content/graphics/ui/btn_restart_level.png" );

    m_extraLife.SetTexture( kuko::ImageManager::GetTexture( "heart" ) );

    if ( Messager::msg["previousState"] == "title" )
    {
        m_levelPath = "content/worlds/";
    }
    else if ( Messager::msg["previousState"] == "level_editor"
        || Messager::msg["previousState"] == "custom_level_select" )
    {
        m_levelPath = "levels/";
    }

    m_levelPath += Messager::msg["level"];
    m_scrollSpeed = 0;

    Logger::Out( "Load level: \"" + Messager::msg["level"] + "\"" );

    m_pauseButton.Setup( "btn_pause",
        kuko::FloatRect( screenWidth - 90, 10, 80, 80 ), false,
        kuko::ImageManager::GetTexture( "btn_pause" ),
        { 0xFF, 0xFF, 0xFF, 0xFF } );

    m_exitButton.Setup( "btn_exit_level",
        kuko::FloatRect( 300, 380, 140, 140 ), false,
        kuko::ImageManager::GetTexture( "btn_exit_level" ),
        { 0xFF, 0xFF, 0xFF, 0xFF } );

    m_restartButton.Setup( "btn_restart_level",
        kuko::FloatRect( 550, 380, 140, 140 ), false,
        kuko::ImageManager::GetTexture( "btn_restart_level" ),
        { 0xFF, 0xFF, 0xFF, 0xFF } );

    m_continueButton.Setup( "btn_continue_level",
        kuko::FloatRect( 800, 380, 140, 140 ), false,
        kuko::ImageManager::GetTexture( "btn_continue_level" ),
        { 0xFF, 0xFF, 0xFF, 0xFF } );

    m_shadebg.SetTexture( kuko::ImageManager::GetTexture( "shade_black" ) );
    m_pausebg.SetTexture( kuko::ImageManager::GetTexture( "bg_paused" ) );
    m_menuHeader.Setup( "menu_header", "asdf", kuko::FloatRect( 520, 160, 100, 70 ),
        true, { 0x00, 0x00, 0x00, 0xFF }, kuko::FontManager::GetFont( "main" ) );

    m_buttonLabel1.Setup( "label_1", "asdf", kuko::FloatRect( 350, 540, 100, 40 ),
        false, { 0x00, 0x00, 0x00, 0xFF }, kuko::FontManager::GetFont( "main" ) );

    m_buttonLabel2.Setup( "label_2", "asdf", kuko::FloatRect( 580, 540, 100, 40 ),
        false, { 0x00, 0x00, 0x00, 0xFF }, kuko::FontManager::GetFont( "main" ) );

    m_buttonLabel3.Setup( "label_3", "asdf", kuko::FloatRect( 820, 540, 100, 40 ),
        false, { 0x00, 0x00, 0x00, 0xFF }, kuko::FontManager::GetFont( "main" ) );

    m_hasExtraLife = false;//true;
    m_hasHitObstacle = false;

    SetupLevel();
    SetupCharacters();

    m_level.SetScroll( 2000 );
    m_effectCounter = 0;

    m_scrollSpeed = -1000;

    m_fin.SetName( "Fin" );
    m_kit.SetName( "Kit" );

    m_subState = PLAYING;

    int song = m_level.GetSongID();
    if ( song == 1 ) { kuko::SoundManager::PlayMusic( "level1", true ); }
    else if ( song == 2 ) { kuko::SoundManager::PlayMusic( "level2", true ); }
    else { kuko::SoundManager::PlayMusic( "level3", true ); }
}

void JuggleGameState::SetupCharacters()
{
    m_fin.Reset();
    m_kit.Reset();

    // SetPhysics( float gravity, float initVelocity, bool canDoubleJump, float maxY )
    float gravity = 600;
    float initVelocityFin = -600;
    float initVelocityKit = -800;

    m_fin.SetTexture( kuko::ImageManager::GetTexture( "fin" ) );
    m_fin.SetPosition( kuko::FloatRect( 50, 550, 266, 336 ) );
    m_fin.SetSolid( true );
    m_fin.SetCollisionRegion( kuko::FloatRect( 75, 0, 150, 75 ) );
    m_fin.SetDefaultFrame(  kuko::IntRect( 1, 1, 266, 336 ) );
    m_fin.SetJumpFrame(     kuko::IntRect( 268, 1, 266, 336 ) );
    m_fin.SetPhysics( gravity, initVelocityFin, false, 550 );

    m_kit.SetTexture( kuko::ImageManager::GetTexture( "kit" ) );
    m_kit.SetPosition( kuko::FloatRect( 145, 450, 120, 116 ) );
    m_kit.SetDoubleJumpAbility( true );
    m_kit.SetSolid( true );
    m_kit.SetCollisionRegion( kuko::FloatRect( 0, 0, 120, 116 ) );
    m_kit.SetDefaultFrame(  kuko::IntRect( 1, 1, 120, 116 ) );
    m_kit.SetJumpFrame(     kuko::IntRect( 122, 1, 120, 116 ) );
    m_kit.SetPhysics( gravity, initVelocityKit, true, 550 );

    m_extraLife.SetPosition( kuko::FloatRect( 25, 550, 80, 80 ) );
}

void JuggleGameState::Cleanup()
{
    Logger::Out( "Cleanup", "JuggleGameState::Cleanup" );
    kuko::ImageManager::ClearTextures();
    m_level.Clear();
}

void JuggleGameState::SetupPauseMenu()
{
    // Draw menu background
    m_menuHeader.ChangeText( kuko::LanguageManager::Text( "game_menu_paused" ) );

    m_buttonLabel1.ChangeText( kuko::LanguageManager::Text( "game_menu_exit" ) );
    m_buttonLabel2.ChangeText( kuko::LanguageManager::Text( "game_menu_restart" ) );
    m_buttonLabel3.ChangeText( kuko::LanguageManager::Text( "game_menu_continue" ) );
}

void JuggleGameState::SetupSuccessMenu()
{
    // Draw menu background
    m_menuHeader.ChangeText( kuko::LanguageManager::Text( "game_menu_success" ) );

    m_buttonLabel1.ChangeText( kuko::LanguageManager::Text( "game_menu_exit" ) );
    m_buttonLabel2.ChangeText( kuko::LanguageManager::Text( "game_menu_restart" ) );
    m_buttonLabel3.ChangeText( kuko::LanguageManager::Text( "game_menu_next_level" ) );

    // Level ranking:
    int stars = 3;
    if ( m_level.GetRemainingTrinketCount() > 0 )
    {
        stars--;
    }

    if ( m_hasHitObstacle )
    {
        stars--;
    }

    Logger::Out( "Score: " + StringUtil::IntToString( stars ) );

    for ( int i = 0; i < 3; i++ )
    {
        kuko::FloatRect pos;
        pos.w = 140;
        pos.h = 140;
        pos.x = i * (pos.w + pos.w / 2) + 340;
        pos.y = 230;

        kuko::IntRect frame;
        frame.x = 0;
        frame.y = 0;
        frame.w = 161;
        frame.h = 159;

        if ( i >= stars )
        {
            frame.x = 161;
        }

        m_scores[i].SetTexture( kuko::ImageManager::GetTexture( "score_icons" ) );
        m_scores[i].SetPosition( pos );
        m_scores[i].SetFrame( frame );
    }
}

void JuggleGameState::SetupFailureMenu()
{
    // Draw menu background
    m_menuHeader.ChangeText( kuko::LanguageManager::Text( "game_menu_failure" ) );

    m_buttonLabel1.ChangeText( kuko::LanguageManager::Text( "game_menu_exit" ) );
    m_buttonLabel2.ChangeText( kuko::LanguageManager::Text( "game_menu_restart" ) );
    //m_buttonLabel3.ChangeText( kuko::LanguageManager::Text( "game_menu_next_level" ) );
}

void JuggleGameState::UpdateMenu()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;

        x /= kuko::Application::GetWidthRatio();
        y /= kuko::Application::GetHeightRatio();

        if ( m_continueButton.IsTriggered( x, y ) )
        {
            m_subState = PLAYING;
        }

        else if ( m_restartButton.IsTriggered( x, y ) )
        {
            m_gotoState = "juggle_game";
        }

        else if ( m_exitButton.IsTriggered( x, y ) )
        {
            m_gotoState = Messager::msg["previousState"];
            Messager::msg["previousState"] = "";
        }
    }
}

void JuggleGameState::UpdatePlaying()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    std::string keyHit = kuko::InputManager::GetKeyHit();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;
        x /= kuko::Application::GetWidthRatio();
        y /= kuko::Application::GetHeightRatio();

        if ( m_fin.IsClicked( x, y ) )
        {
            FinJump();
        }
        else if ( m_kit.IsClicked( x, y ) )
        {
            KitJump();
        }

        if ( m_pauseButton.IsTriggered( x, y ) )
        {
            m_subState = PAUSED;
            SetupPauseMenu();
        }
    }
    else if ( input[ kuko::MOVE_UP ].down )
    {
        KitJump();
    }
    else if ( input[ kuko::MOVE_DOWN ].down )
    {
        FinJump();
    }
    else if ( input[ kuko::BACKSPACE ].down )
    {
        // TODO: TEMP
        m_gotoState = "level_editor";
    }

    else if ( keyHit == "1" )
    {
        // DEBUG
        Logger::Debug( "DEBUG INFORMATION", "JuggleGameState::UpdatePlaying" );
        Logger::Debug( "Fin Location: "
            + StringUtil::FloatToString( m_fin.GetPosition().x ) + ", "
            + StringUtil::FloatToString( m_fin.GetPosition().y )
            + " Fin Dimensions: "
            + StringUtil::FloatToString( m_fin.GetPosition().w ) + " x "
            + StringUtil::FloatToString( m_fin.GetPosition().h )
            + "\t Kit Location: "
            + StringUtil::FloatToString( m_kit.GetPosition().x ) + ", "
            + StringUtil::FloatToString( m_kit.GetPosition().y )
            + " Kit Dimensions: "
            + StringUtil::FloatToString( m_kit.GetPosition().w ) + " x "
            + StringUtil::FloatToString( m_kit.GetPosition().h )
            , "JuggleGameState::UpdatePlaying" );

        m_level.TileDebugOut();

        Logger::Debug( "Step Counter: "
            + StringUtil::FloatToString( kuko::Application::GetTimeStep() )
            , "JuggleGameState::UpdatePlaying" );
    }

    HandleCollisions();

    ScrollLevel();
    m_fin.Update( kuko::Application::GetTimeStep() );
    m_kit.Update( kuko::Application::GetTimeStep() );

    if ( m_kit.IsJumping() || m_fin.IsJumping() )
    {
    }

    if ( !m_kit.IsCollision( m_fin ) )
    {
        m_kit.Fall( kuko::Application::GetTimeStep() );
    }
    else
    {
        m_kit.HitGround();

        if (
            m_kit.GetPosition().y + m_kit.GetCollisionRegion().h >
            m_fin.GetPosition().y )
        {
            m_kit.PushUp( m_fin );
        }
    }
}

void JuggleGameState::Update()
{
    if ( m_subState == PLAYING )
    {
        UpdatePlaying();
    }
    else
    {
        UpdateMenu();
    }
}

void JuggleGameState::FinJump()
{
    if ( m_kit.IsJumping() )
    {
//        kuko::SoundManager::PlaySound( "jump_fin" );
        m_fin.BeginJump();
    }
    else
    {
        m_fin.Launch();
        KitJump();
    }
}

void JuggleGameState::KitJump()
{
//    kuko::SoundManager::PlaySound( "jump_kit" );
    m_kit.BeginJump();
}

void JuggleGameState::ScrollLevel()
{
    float scrollSpeed = m_scrollSpeed * kuko::Application::GetTimeStep();

    m_level.Scroll( scrollSpeed );
    m_level.ScrollBackground( scrollSpeed );
    m_level.Update();

    m_effectCounter += 1;
    float rotateAmount = 0.1;
    if ( m_effectCounter >= 1000 ) { m_effectCounter = 0; }

    // Heart animation
    float rotation = m_extraLife.GetRotation();

    if ( m_effectCounter < 250 )
    {
        rotation += rotateAmount;
    }
    else if ( m_effectCounter < 500 )
    {
        rotation -= rotateAmount;
    }
    else if ( m_effectCounter < 750 )
    {
        rotation -= rotateAmount;
    }
    else
    {
        rotation += rotateAmount;
    }

    m_extraLife.SetRotation( rotation );
}

void JuggleGameState::HandleCollisions()
{
    ItemCategory kitHit = m_level.Collision( m_kit );
    ItemCategory finHit = m_level.Collision( m_fin );

    if ( finHit != NONE || kitHit != NONE )
    {
        Logger::Out( "Kit hit " + StringUtil::IntToString( kitHit ) + " Fin hit " + StringUtil::IntToString( finHit ) );
    }

    if ( kitHit == TRINKET || finHit == TRINKET )
    {
        kuko::SoundManager::PlaySound( "collect_good" );
    }
    if ( kitHit == OBSTACLE || finHit == OBSTACLE )
    {
        m_hasHitObstacle = true;

        if ( m_hasExtraLife )
        {
            kuko::SoundManager::PlaySound( "collect_bad" );
            m_hasExtraLife = false;
        }
        else
        {
            kuko::SoundManager::PlaySound( "collect_dead" );
            m_subState = LEVEL_FAIL;
            SetupFailureMenu();
        }
    }
    if ( kitHit == EXTRA_LIFE || finHit == EXTRA_LIFE )
    {
        m_hasExtraLife = true;
    }
    if ( kitHit == LEVEL_END || finHit == LEVEL_END )
    {
        Logger::Out( "Level end" );
        m_subState = LEVEL_SUCCESS;
        SetupSuccessMenu();
    }
    if ( kitHit == UNSET || finHit == UNSET )
    {
        Logger::Error( "Something hit object of unset type", "JuggleGameState::HandleCollisions" );
    }
}

void JuggleGameState::Draw()
{
    m_level.Draw();
    m_fin.Draw();
    m_kit.Draw();

    if ( m_hasExtraLife )
    {
        m_extraLife.Draw();
    }

    m_level.DrawForeground();

    m_pauseButton.Draw();

    if ( m_subState == PAUSED || m_subState == LEVEL_FAIL || m_subState == LEVEL_SUCCESS )
    {
        m_shadebg.Draw();
        m_pausebg.Draw();
        m_restartButton.Draw();
        m_exitButton.Draw();
        m_menuHeader.Draw();
        m_buttonLabel1.Draw();
        m_buttonLabel2.Draw();
    }
    if ( m_subState == PAUSED || m_subState == LEVEL_SUCCESS )
    {
        // Don't draw the "next" or "resume" button if player failed the level!
        m_continueButton.Draw();
        m_buttonLabel3.Draw();
    }
    if ( m_subState == LEVEL_SUCCESS )
    {
        for ( int i = 0; i < 3; i++ )
        {
            m_scores[i].Draw();
        }
    }
}

void JuggleGameState::SetupLevel()
{
    m_level.Load( m_levelPath );

    int width = kuko::Application::GetDefaultWidth();
    int height = kuko::Application::GetDefaultHeight();

    m_level.Begin();
}





