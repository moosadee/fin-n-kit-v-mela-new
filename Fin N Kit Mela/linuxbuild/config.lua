config = {
	borderless = "false",
	fullscreen = "false",
	language = "english",
	music_volume = "100",
	savegame_count = "0",
	screen_height = "720",
	screen_width = "1280",
	sound_volume = "100",
	vsync = "true",
}
